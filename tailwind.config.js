module.exports = {
  purge: { content: ["./public/**/*.html", "./src/**/*.vue"] },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: "#5BA4A4FF",
        secondary: {
          100:'#EFFAFAFF',
          200: '#EEF6F6FF',
          300: '#7B8E8EFF',
          400: '#2C3A3AFF'
        }
      },
      fontFamily: {
        'body': ["Spartan"],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
