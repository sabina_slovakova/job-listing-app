import { ref } from "vue";

const getData = () => {
  let jobs = ref([])
  let job = ref([])

  const getAllJobs = async () => {
    try {
      let res = await fetch('http://localhost:3000/jobs')
      jobs.value = await res.json()
    } catch (err) {
      console.log(err.message);
    }
  }

  const getJob = async (id) => {
    try {
      let res = await fetch('http://localhost:3000/jobs/' + id)
      job.value = await res.json()
    } catch (err) {
      console.log(err.message);
    }

  }


  return { getAllJobs, getJob, jobs, job }
}

export default getData
